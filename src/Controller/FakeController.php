<?php

namespace App\Controller;
use Faker;
use App\Entity\Categorie;
use App\Entity\Ticket;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface ;


class FakeController extends AbstractController
{

/**
* @Route("/test-constraint", name="faker-ticket")
*/

public function testContraint(ValidatorInterface $validator)
{

  $entityManager = $this->getDoctrine()->getManager();

  $ticket = new Ticket();
  $ticket->setTitre('titre');
  $ticket->setDate( new \DateTime());
  $ticket->setDescription('description');


  $categorie = new Categorie();
  $categorie->setNom('nom');
  $ticket->setCategorie($categorie);
  $errors = $validator->validate($ticket);

  $entityManager->persist($categorie);
  $entityManager->flush();

  $entityManager->persist($ticket);
  $entityManager->flush();

  echo'<pre>';
  echo dump($errors);
  echo '</pre>';

  return new Response('Tickets validating ....');
}
}
